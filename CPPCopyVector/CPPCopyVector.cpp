﻿#include "pch.h"
#include <iostream>
#include <vector>

int main()
{
	// コピー元のvectorを用意します。
	std::vector<int> src = { 1, 5, 13, 21 };

	// コピー先のvectorを用意します。
	std::vector<int> dst;

	// コピー先のvectorのサイズをコピー元のサイズに設定する必要があります。
	dst.resize(src.size());

	// vectorをコピーします。
	std::copy(src.begin(), src.end(), dst.begin());

	for (const auto& item : dst)
	{
		std::cout << item << std::endl;
	}
}
